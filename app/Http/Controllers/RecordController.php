<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Record;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    //
    public function addNewRecord(Request $request)
    {
        # code...
        // dd();
        $record = new Record;
        $record->name = $request->name;
        $record->lastName = $request->lastName;
        $record->save();
        // return $request;
    }

    public function getRecords()
    {
        return Record::get();   
    }

    public function delete(Request $request)
    {
        # code...
        Record::where('id', $request->id)->delete();
    }

    public function update(Request $request)
    {
        # code...
        $record = Record::find($request->id); 
        $record->name = $request->name;
        $record->lastName = $request->lastName;
        $record->save();
    }
}
