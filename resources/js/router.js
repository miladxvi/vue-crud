import Vue from 'vue';
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Records from './views/Records.vue'

Vue.use(VueRouter);

const NotFoundComponent = {
    template: "<p> Page Not Found </p>"
}

const routes = [{
        path: '/',
        name: 'home',
        component: Home

    },
    {
        path: '/records',
        name: 'records',
        component: Records
    }
]

const router = new VueRouter({
    // data: () => {
    //     currentRoute: window.location.pathname
    // },
    // computed:{
    //     CurrentComponent(){
    //         return routes[this.currentRoute] || NotFoundComponent;
    //     }
    // },

    // render(h) {
    //     return Vue.h(this.CurrentComponent);
    // },
    routes
})

const app = new Vue({
    router
}).$mount("#app");
